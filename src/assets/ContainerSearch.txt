          <mat-sidenav-container class="sidenav-container">
            <mat-sidenav
              #drawer
              class="sidenav"
              fixedInViewport
              [attr.role]="(isHandset$ | async) ? 'dialog' : 'navigation'"
              [mode]="(isHandset$ | async) ? 'over' : 'side'"
              [opened]="(isHandset$ | async) === false"
            >
              <br />
              <mat-toolbar>Filtre de recherche</mat-toolbar>
              <mat-nav-list>
                <!-- <br>
                <br> -->
          
                <mat-toolbar>Transport</mat-toolbar>
                <br />
                <br />
                <mat-label>Prix du transport</mat-label>
                <br />
          
                <mat-form-field appearance="legacy">
                  <mat-label>min</mat-label>
                  <input matInput type="number" min="0" />
                </mat-form-field>
          
                <mat-form-field appearance="legacy">
                  <mat-label>max</mat-label>
                  <input matInput type="number" [min]="max" />
                </mat-form-field>
          
                <br />
                <mat-form-field appearance="legacy">
                  <mat-label>Eyang </mat-label>
                  <mat-select>
                    <mat-option value="depart">départ</mat-option>
                    <mat-option value="arrivée">arrivée</mat-option>
                  </mat-select>
                </mat-form-field>
                <br />
                <mat-panel-title>Date de départ</mat-panel-title>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-label>
                    <input matInput type="date" />
                  </mat-label>
                </mat-form-field>
          
                <br />
                <br />
                <mat-panel-title> Heure de départ </mat-panel-title>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-label>
                    <input matInput type="time" />
                  </mat-label>
                </mat-form-field>
          
                <br />
                <br />
                <mat-toolbar> Achat </mat-toolbar>
                <br />
                <br />
                <mat-label>Prix du produit</mat-label>
                <br />
          
                <mat-form-field appearance="legacy">
                  <mat-label>min</mat-label>
                  <input matInput type="number" min="0" />
                </mat-form-field>
          
                <mat-form-field appearance="legacy">
                  <mat-label>max</mat-label>
                  <input matInput type="number" [min]="max" />
                </mat-form-field>
                <br />
                <br />
                <mat-panel-title> Lieu d'achat du produit </mat-panel-title>
                <br />
                <mat-form-field appearance="legacy">
                  <input matInput type="text" />
                </mat-form-field>
                <br />
                <mat-panel-title> Quantité du produit </mat-panel-title>
                <br />
                <mat-form-field appearance="legacy">
                  <input matInput type="number" min="1" />
                </mat-form-field>
                <br />
                <mat-panel-title> categorie de l'Achat </mat-panel-title>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-select>
                    <mat-option
                      *ngFor="let categorie of categories"
                      [value]="categorie.valeur"
                      >{{ categorie.valeur }}</mat-option
                    >
                  </mat-select>
                </mat-form-field>
          
                <mat-toolbar> Covoiturage </mat-toolbar>
                <br />
                <br />
                <mat-label> type de Covoiturage </mat-label>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-select>
                    <mat-option *ngFor="let c of typeco" [value]="c"> {{ c }}</mat-option>
                  </mat-select>
                </mat-form-field>
                <br />
                <br />
                <mat-label> lieu de départ </mat-label>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-select>
                    <mat-option *ngFor="let l of lieu" [value]="l"> {{ l }}</mat-option>
                  </mat-select>
                </mat-form-field>
                <br />
                <mat-label> Heure de départ </mat-label>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-label>
                    <input matInput type="time" />
                  </mat-label>
                </mat-form-field>
                <br />
                <mat-label> lieu d'arrivée </mat-label>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-select>
                    <mat-option *ngFor="let li of lieu" [value]="li">{{ li }}</mat-option>
                    <mat-option value="autre"> autre</mat-option>
                  </mat-select>
                </mat-form-field>
                <br />
                <mat-label> places disponible </mat-label>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-label>
                    <input matInput type="number" min="1" />
                  </mat-label>
                </mat-form-field>
                <hr />
                <div style="text-align: center">
                  <button
                    type="button"
                    id="submit"
                    name="submit"
                    class="btn btn-primary"
                    style="
                      margin-bottom: 12px;
                      border-radius: 45px;
                      padding: 10px 30 10px 30px;
                    "
                  >
                    Filtrer
                  </button>
                </div>
              </mat-nav-list>
            </mat-sidenav>
            <mat-sidenav-content>
              <mat-toolbar color="primary">
                <button
                  type="button"
                  aria-label="Toggle sidenav"
                  mat-icon-button
                  (click)="drawer.toggle()"
                  *ngIf="isHandset$ | async"
                >
                  <mat-icon aria-label="Side nav toggle icon">menu</mat-icon>
                </button>
              </mat-toolbar>
            </mat-sidenav-content>
          </mat-sidenav-container>






























































































          
            <mat-radio-group
              aria-labelledby="example-radio-group-label"
              class="example-radio-group"
              [(ngModel)]="services"
            >
              <label>Achats</label>
              <mat-radio-button
                class="example-radio-button"
                [value]="achat"
                (change)="disableOthers('achat')"
              >
              </mat-radio-button>
              <label>Covoiturage</label>
              <mat-radio-button
                class="example-radio-button"
                [value]="covoiturage"
                (change)="disableOthers('covoiturage')"
              >
              </mat-radio-button>
              <label>Transport</label>
              <mat-radio-button
                class="example-radio-button"
                [value]="transport"
                (change)="disableOthers('transport')"
              >
              </mat-radio-button>
            </mat-radio-group>
            <br /><br />
            <div style="background-color: rgb(182, 179, 179); border-radius: 30px; max-width: 35%;">
              <div class="container" *ngIf="achat">
                <h2> Achat </h2>
                <mat-label>Prix du produit</mat-label>
                <br />
          
                <mat-form-field appearance="legacy">
                  <mat-label>min</mat-label>
                  <input matInput type="number" min="0" />
                </mat-form-field>
          <br>
                <mat-form-field appearance="legacy">
                  <mat-label>max</mat-label>
                  <input matInput type="number" [min]="max" />
                </mat-form-field>
                <br />
                <br />
                <mat-panel-title> Lieu d'achat du produit </mat-panel-title>
                <br />
                <mat-form-field appearance="legacy">
                  <input matInput type="text" />
                </mat-form-field>
                <br />
                <mat-panel-title> Quantité du produit </mat-panel-title>
                <br />
                <mat-form-field appearance="legacy">
                  <input matInput type="number" min="1" />
                </mat-form-field>
                <br />
                <mat-panel-title> categorie de l'Achat </mat-panel-title>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-select>
                    <mat-option
                      *ngFor="let categorie of categories"
                      [value]="categorie.valeur"
                      >{{ categorie.valeur }}</mat-option
                    >
                  </mat-select>
                </mat-form-field>
                <div style="text-align:right">
                  <button
                    type="button"
                    id="submit"
                    name="submit"
                    class="btn btn-primary"
                    style="
                      margin-bottom: 12px;
                      border-radius: 45px;
                      padding: 0px 45px 0px 45px;
                    "
                  >
                    Filtrer
                  </button>
                </div>
           
          
              </div>

              <div class="container" *ngIf="covoiturage">
                <h2> Covoiturage </h2>
                <mat-label> type de Covoiturage </mat-label>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-select>
                    <mat-option *ngFor="let c of typeco" [value]="c"> {{ c }}</mat-option>
                  </mat-select>
                </mat-form-field>
                <br />
                <br />
                <mat-label> lieu de départ </mat-label>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-select>
                    <mat-option *ngFor="let l of lieu" [value]="l"> {{ l }}</mat-option>
                  </mat-select>
                </mat-form-field>
                <br />
                <mat-label> Heure de départ </mat-label>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-label>
                    <input matInput type="time" />
                  </mat-label>
                </mat-form-field>
                <br />
                <mat-label> lieu d'arrivée </mat-label>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-select>
                    <mat-option *ngFor="let li of lieu" [value]="li">{{ li }}</mat-option>
                    <mat-option value="autre"> autre</mat-option>
                  </mat-select>
                </mat-form-field>
                <br />
                <mat-label> places disponible </mat-label>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-label>
                    <input matInput type="number" min="1" />
                  </mat-label>
                </mat-form-field>
                <div style="text-align:right">
                  <button
                    type="button"
                    id="submit"
                    name="submit"
                    class="btn btn-primary"
                    style="
                      margin-bottom: 12px;
                      border-radius: 45px;
                      padding: 0px 45px 0px 45px;
                    "
                  >
                    Filtrer
                  </button>
                </div>
              </div>

              <div class="container" *ngIf="transport">
                <h2>Transport</h2>
                <mat-label>Prix du transport</mat-label>
                <br />
          
                <mat-form-field appearance="legacy">
                  <mat-label>min</mat-label>
                  <input matInput type="number" min="0" />
                </mat-form-field>
          <br>
                <mat-form-field appearance="legacy">
                  <mat-label>max</mat-label>
                  <input matInput type="number" [min]="max" />
                </mat-form-field>
          
                <br />
                <mat-form-field appearance="legacy">
                  <mat-label>Eyang </mat-label>
                  <mat-select>
                    <mat-option value="depart">départ</mat-option>
                    <mat-option value="arrivée">arrivée</mat-option>
                  </mat-select>
                </mat-form-field>
                <br />
                <mat-panel-title>Date de départ</mat-panel-title>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-label>
                    <input matInput type="date" />
                  </mat-label>
                </mat-form-field>
          
                <br />
                <br />
                <mat-panel-title> Heure de départ </mat-panel-title>
                <br />
                <mat-form-field appearance="legacy">
                  <mat-label>
                    <input matInput type="time" />
                  </mat-label>
                </mat-form-field>
                <div style="text-align:right">
                  <button
                    type="button"
                    id="submit"
                    name="submit"
                    class="btn btn-primary"
                    style="
                      margin-bottom: 12px;
                      border-radius: 45px;
                      padding: 0px 45px 0px 45px;
                    "
                  >
                    Filtrer
                  </button>
                </div>
              </div>

            </div>

