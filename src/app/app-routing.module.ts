import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { NewpostComponent } from './components/newpost/newpost.component';
import { SearchComponent } from './components/search/search.component';
import { RegisterComponent } from './components/register/register.component';
import { LogInComponent } from './components/log-in/log-in.component';
// import { ListBookComponent } from './admin/list-book/list-book.component';
// import { ListOffersComponent } from './admin/list-offers/list-offers.component';
// import { AdminComponent } from './admin/admin.component';
import { LandingComponent } from './components/landing/landing.component';
import { NotifsComponent } from './components/notifs/notifs.component';




const routes: Routes = [
  {path:'', component:NewpostComponent},
  {path:'home', component:HomeComponent},
  {path: 'newpost', component:NewpostComponent},
  {path: 'search', component:SearchComponent},
  {path: 'register', component:RegisterComponent},
  {path: 'login', component:LogInComponent},
  {path: 'landing', component:LandingComponent},
  {path: 'notification', component:NotifsComponent},
  { 
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule) 
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
