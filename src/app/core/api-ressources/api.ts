export class Api {

    public readonly SIGN_IN_API: string = "http://localhost:8080/signin";
    public readonly SINGN_UP_API: string = "http://localhost:8080/signup";

    public readonly FOLLOW: string = "http://localhost:8080/follow";

    public readonly PUBLISH_PURCHASE_API: string = "http://localhost:8080/post/purchase";
    public readonly PUBLISH_POOLING_API: string = "http://localhost:8080/post/pooling";
    public readonly PUBLISH_TRANSPORT_API: string = "http://localhost:8080/post/transport";

    public readonly DELETE_PURCHASE_API: string = "http://localhost:8080/delete/purchase";
    public readonly DELETE_POOLING_API: string = "http://localhost:8080/delete/pooling";
    public readonly DELETE_TRANSPORT_API: string = "http://localhost:8080/delete/transport/";

    public readonly FIND_PURCHASE_API: string = "http://localhost:8080/find/purchase";
    public readonly FIND_POOLING_API: string = "http://localhost:8080/find/pooling";
    public readonly FIND_TRANSPORT_API: string = "http://localhost:8080/find/transport";

    public readonly LIST_SERVICE_API: string = "http://localhost:8080/find/services";
    public readonly LIST_SERVICE_OWNER_API: string = "http://localhost:8080/admin/services";

    public readonly BOOK_API:string = "http://localhost:8080/create/book";
    public readonly LIST_BOOK_API:string ="http://localhost:8080/list/book";
    public readonly CONFIRM_BOOK_API:string = "http://localhost:8080/confirm/book";
    public readonly DELETE_BOOK_API:string = "http://localhost:8080/delete/book";

    public readonly RETRIEVE_NOTIFICATION: string = "http://localhost:8080/get/notifications";
    public readonly UPDATE_STATUS_NOTIFICATION: string = "http://localhost:8080/update/notification";

}
