export class KeyStorage {
    public readonly TOKEN_NAME: string = "TOKEN";
    // The key for the storage of the books of the service offer we read
    public readonly BOOKS_SERVICE_KEY: string = "BOOKS_SERVICE";

    public readonly USER_LOGIN: string = "LOGIN";
}
