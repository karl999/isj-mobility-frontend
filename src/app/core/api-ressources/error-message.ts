export class ErrorMessage {

    public readonly messageCode: {[key: number]: string} = {

        // 200 => ok
        200: "",

        // the code which start with 4 is error code
        // 41 => invalid
        410: "Nom d'utilisateur/ mot de passe invalide",
        411: "Ce nom d'utilisateur/cet adresse mail est déjà utilisé",
        420: "Votre session a expiré",
        // 44 => non-existennt
        440: "cet utilisateur est introuvable",
        441: "Notfication introuvable",
        442: "L'offre introuvable",
        443: "Type de covoiturage introuvable",
        446: "La catégorie de produit est introuvable",
        447: "Le statut de cette réservation introuvable",
        448: "Ce service introuvable",
        449: "Cette réservation introuvable",

        450: "Le nombre de réservations ne peux excéder le nombre de places disponibles",
        452: "L'offre n'a pas pu être créée",
        454: "Action impossible, vous êtes le propriétaire de ce service",
        // The code which start with 46 is the request is forbidden
        460: "Vous n'avez pas le droit de supprimer cet offre",
        461: "Vous n'avez pas le droit de supprimer cette réservation",
        // generic error
        495: "Une erreur s'est produite",

        // The code which start with 5 is the request is successful
        // 52 => création
        520: "L'offre a été bien crée",
        522: "L'offre a été bien supprimée",
        // 53 => deletion
        533: "Service réservé avec succès",
        534: "La réservation a été bien supprimée",
        // 56 => Confirmation
        561: "La réservation a été confirmée",
        // 59 => follow
        590: "Vous suivez désormais cet utilisateur",
    };

}
