import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from '../angular-material-module/angular-material.module';

import { ErrorMessage } from './api-ressources/error-message';
import { KeyStorage } from './api-ressources/key-storage';
import { Api } from './api-ressources/api';
import { Pooling } from './models/pooling/pooling';
import { Purchase } from './models/purchase/purchase';
import { Book } from './models/book/book.model';
import { TimeClass } from './models/time/time-class';
import { Transport } from './models/transport/transport';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AngularMaterialModule,
  ],
  providers: [
    Api,
    ErrorMessage,
    KeyStorage,
    Pooling,
    Purchase,
    Book,
    TimeClass,
    Transport,
  ],
})
export class CoreModule { }
