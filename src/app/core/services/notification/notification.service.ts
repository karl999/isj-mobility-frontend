import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Api } from '../../api-ressources/api';
import { KeyStorage } from '../../api-ressources/key-storage';
import { ResponseDto } from '../../dto/response-dto';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    private httpClient: HttpClient,
    private apiUrl: Api,
    private keyStorage: KeyStorage,
  ) { }


  private httpOptions = {
    headers : new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem(this.keyStorage.TOKEN_NAME),
    })
  };

  follow(login: string): Observable<ResponseDto>{
    return this.httpClient.get<ResponseDto>(this.apiUrl.FOLLOW,this.httpOptions);
  }


}
