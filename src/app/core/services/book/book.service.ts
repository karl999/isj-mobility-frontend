import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Api } from '../../api-ressources/api';
import { KeyStorage } from '../../api-ressources/key-storage';
import { BookResponse } from '../../dto/book-response';
import { Book } from '../../models/book/book.model';
import { ResponseDto } from '../../dto/response-dto';
import { StatutReservation } from '../../enum/statut-reservation';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(
    private httpClient: HttpClient,
    private apiUrl: Api,
    private keyStorage: KeyStorage,
  ) { }

  private httpOptions = {
    headers : new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem(this.keyStorage.TOKEN_NAME),
    })
  };


  public book(code: string, qte: number = 0): Observable<ResponseDto>{
    let apiUrl = qte == 0 ? this.apiUrl.BOOK_API + "?c=" + code : this.apiUrl.BOOK_API + "?c=" + code + "q=" + qte;
    return this.httpClient.get<ResponseDto>(apiUrl,this.httpOptions);
  }

  public deleteBook(code: string): Observable<ResponseDto>{
    let apiUrl =  this.apiUrl.DELETE_BOOK_API + "/" + code 
    return this.httpClient.get<ResponseDto>(apiUrl,this.httpOptions);
  }

  public confirmBook(dataBook:{ c:string, statut: StatutReservation }): Observable<ResponseDto>{
    return this.httpClient.post<ResponseDto>(this.apiUrl.CONFIRM_BOOK_API,dataBook,this.httpOptions);
  }

  public listBook(code:string): Observable<BookResponse>{
    let apiUrl = this.apiUrl.LIST_BOOK_API + "/" + code;
    return this.httpClient.get<BookResponse>(apiUrl,this.httpOptions);
  }

  /*
  .subscribe(
    response =>{
      if(!response.error){
        response.books.forEach(book =>{
          reservation.deserialize(book);
          this.booksList.push(reservation);
        })
      }
      this.snackBar.open(this.errorMessage.messageCode[response.requestStatus]);
    }
  );

  .subscribe(
      response =>{
        this.snackBar.open(this.errorMessage.messageCode[response.requestStatus]);
      }
    );
  */

}
