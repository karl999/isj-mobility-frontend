import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Api } from '../../api-ressources/api';
import { KeyStorage } from '../../api-ressources/key-storage';
import { FindServiceResponse } from '../../dto/find-service-response';
import { ResponseDto } from '../../dto/response-dto';
import { Pooling } from '../../models/pooling/pooling';
import { Purchase } from '../../models/purchase/purchase';
import { Transport } from '../../models/transport/transport';
import { Observable } from 'rxjs';
import { PoolingDto } from '../../dto/pooling/pooling-dto';
import { PurchaseDto } from '../../dto/purchase/purchase-dto';
import { TransportDto } from '../../dto/transport/transport-dto';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(
    private httpClient: HttpClient,
    private apiUrl: Api,
    private keyStorage: KeyStorage,
  ) { }

  private httpOptions = {
    headers : new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem(this.keyStorage.TOKEN_NAME),
    })
  };


  publishPoolingOffer(poolingOffer: PoolingDto): Observable<ResponseDto>{
    return this.httpClient.post<ResponseDto>(this.apiUrl.PUBLISH_POOLING_API, poolingOffer, this.httpOptions);
  }

  publishPurchaseOffer(purchaseOffer: PurchaseDto): Observable<ResponseDto>{
    return this.httpClient.post<ResponseDto>(this.apiUrl.PUBLISH_PURCHASE_API, purchaseOffer, this.httpOptions);
  }

  publishtransportOffer(transportOffer: TransportDto): Observable<ResponseDto>{
    let apiUrl = this.apiUrl.PUBLISH_POOLING_API;
    return this.httpClient.post<ResponseDto>(this.apiUrl.PUBLISH_TRANSPORT_API, transportOffer, this.httpOptions);
  }

  findPoolingOffer(page: number = 0, limit: number = 20, pattern: string = ""): Observable<FindServiceResponse<Pooling>>{
    let apiUrl = pattern.length==0 ? this.apiUrl.FIND_POOLING_API + "?page=" + (page-1) + "&limit=" + limit : this.apiUrl.FIND_POOLING_API + "?page=" + page + "&limit=" + limit + "&s=" + pattern ;
    return this.httpClient.get<FindServiceResponse<Pooling>>(apiUrl, this.httpOptions);
  }

  findPurchaseOffer(page: number = 0, limit: number = 20, pattern: string = ""): Observable<FindServiceResponse<Purchase>>{
    let apiUrl = pattern.length==0 ? this.apiUrl.FIND_PURCHASE_API + "?page=" + (page-1) + "&limit=" + limit : this.apiUrl.FIND_PURCHASE_API + "?page=" + page + "&limit=" + limit + "&s=" + pattern;
    return this.httpClient.get<FindServiceResponse<Purchase>>(apiUrl, this.httpOptions);
  }

  findTransportOffer(page: number = 0, limit: number = 20, pattern: string = ""): Observable<FindServiceResponse<Transport>>{
    let apiUrl = pattern.length==0 ? this.apiUrl.FIND_TRANSPORT_API + "?page=" + (page-1) + "&limit=" + limit : this.apiUrl.FIND_TRANSPORT_API + "?page=" + page + "&limit=" + limit + "&s=" + pattern;
    return this.httpClient.get<FindServiceResponse<Transport>>(apiUrl, this.httpOptions);
  }

  deletePoolingOffer(code: string): Observable<ResponseDto>{
    let apiUrl =  this.apiUrl.DELETE_POOLING_API + "/" + code 
    return this.httpClient.get<ResponseDto>(apiUrl,this.httpOptions);
  }

  deletePurchaseOffer(code: string): Observable<ResponseDto>{
    let apiUrl =  this.apiUrl.DELETE_PURCHASE_API + "/" + code 
    return this.httpClient.get<ResponseDto>(apiUrl,this.httpOptions);
  }

  deletetransportOffer(code: string): Observable<ResponseDto>{
    let apiUrl =  this.apiUrl.DELETE_TRANSPORT_API + "/" + code 
    return this.httpClient.get<ResponseDto>(apiUrl,this.httpOptions);
  }

  listServicesOwnerOffer(page: number = 0, limit: number = 20): Observable<FindServiceResponse<Pooling | Purchase | Transport>>{
    let apiUrl = this.apiUrl.LIST_SERVICE_OWNER_API + "?page=" + page +"&limit=" + limit;
    return this.httpClient.get<FindServiceResponse<Pooling | Purchase | Transport>>(apiUrl,this.httpOptions);
  }

  listServicesOffer(page: number = 0, limit: number = 20): Observable<FindServiceResponse<Pooling | Purchase | Transport>>{
    let apiUrl = this.apiUrl.LIST_SERVICE_API + "?page=" + page +"&limit=" + limit;
    return this.httpClient.get<FindServiceResponse<Pooling | Purchase | Transport>>(apiUrl,this.httpOptions);
  }


}
