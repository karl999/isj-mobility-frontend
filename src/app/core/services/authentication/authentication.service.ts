import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable} from 'rxjs';
import { Api } from '../../api-ressources/api';
import { KeyStorage } from '../../api-ressources/key-storage';
import {AuthenticationResponse} from '../../dto/authentication-response'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  token = '';

  constructor(
      private http: HttpClient,
      private router: Router,

      private apiUrl: Api,
      private keyStorage: KeyStorage,
    ) {
    this.loadToken();
  }



  async loadToken() {
    const token = await localStorage.getItem(this.keyStorage.TOKEN_NAME);
    if (token != null && token != '' ) {
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
  }

  login(credentials: {login: string, password: string}): Observable<AuthenticationResponse> {
    const httpOptions = {
      headers : new HttpHeaders({
        'Accept': 'application/json',
      })
    };
    return this.http.post<AuthenticationResponse>(this.apiUrl.SIGN_IN_API, credentials, httpOptions);
  }

  logout(){
    this.isAuthenticated.next(false);
    localStorage.removeItem("TOKEN");
    this.router.navigate(['/login']);
  }

  signUp(userData:{name:string, firstname:string ,login:string ,password:string , email:string, dateN:Date}): Observable<AuthenticationResponse>{
    const httpOptions = {
      headers : new HttpHeaders({
        'Accept': 'application/json',
      })
    };
    return this.http.post<AuthenticationResponse>(this.apiUrl.SINGN_UP_API, userData, httpOptions);

  }
}
