import { TestBed } from '@angular/core/testing';

import { SearchPatternService } from './search-pattern.service';

describe('SearchPatternService', () => {
  let service: SearchPatternService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchPatternService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
