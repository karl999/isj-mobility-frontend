import { Time } from '@angular/common';
import { Injectable } from '@angular/core';
import { TimeClass } from '../../models/time/time-class';

@Injectable({
  providedIn: 'root'
})
export class SearchPatternService {

  constructor() { }

  buildSearchPatternPurchase(data:{[key: string]: string}): string{
    let searchPattern: string = "";
    let date = {
      date: "",
      horaire: "",
    }
    Object.entries(data).forEach(([key, value]) => {
      if(value.length != 0){
        if(key == "categorieProduit"){

        }
        if(key == "dateLivraison"){
          date.date = value;
          
        }
        if(key == "horaireLivraison"){
          date.date = value;
        }
      }
    });
    if(date.date.length != 0){
      searchPattern += this.buildSearchPatternDate(date,"dateLivraison", "<");
    }
    return searchPattern;
  }

  buildSearchPatternPooling(data: any): string{
    let searchPattern: string = "";
    let date = {
      date: "",
      horaire: "",
    }


    return searchPattern;
  }

  buildSearchPatternTransport(data: any): string{
    let searchPattern: string = "";
    let date = {
      date: "",
      horaire: "",
    }


    return searchPattern;
  }

  buildSearchPatternDate(date:{date: string, horaire:string}, fieldName:string, operation:string): string{
    let datec = new Date(date.date + date.horaire).toLocaleDateString();
    return fieldName + operation + datec;
  }
}
