export enum StatutReservation {
    ACCEPTE = "ACCEPTE",
    EN_ATTENTE = "EN_ATTENTE",
    REFUSE = "REFUSE",
}
