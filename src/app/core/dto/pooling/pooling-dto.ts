import { Time } from "@angular/common";
import { TimeClass } from "../../models/time/time-class";

export class PoolingDto {
    constructor(){
        this.prixService = 0.0;
        this.type = "";
        this.lieuDepart = "";
        this.lieuArrivee = "";
        this.placesDispo = 0;
        this.dateDepart = new Date();
        this.horaireDepart = new TimeClass();
    }

    
    type: string;
    prixService: number;
    placesDispo: number;
    lieuDepart: string;
    dateDepart: Date ;
    horaireDepart: Time;
    lieuArrivee: string ;
}
