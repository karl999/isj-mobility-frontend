import { Book } from "../models/book/book.model";

export interface BookResponse {

    error: boolean;
    requestStatus: number;
    books: Array<Book>;
}
