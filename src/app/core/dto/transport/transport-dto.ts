import { Time } from "@angular/common";
import { TimeClass } from "../../models/time/time-class";

export class TransportDto {

    constructor(){
        this.prixService = 0.0;
        this.produit = "";
        this.categorieProduit = "";
        this.pointRamassage = "";
        this.quantite = 0;
        this.lieuLivraison = "";
        this.dateLivraison = new Date();
        this.horaireLivraison = new TimeClass();
    }

    categorieProduit: string;
    produit: string;
    quantite: number;
    pointRamassage: string;
    lieuLivraison: string;
    dateLivraison: Date;
    horaireLivraison: Time;
    prixService: number;
}


