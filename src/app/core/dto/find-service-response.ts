export interface FindServiceResponse<T> {
    error: boolean;
    requestStatus: number;
    offers: {
        content: Array<T>;
        pageable:{
            sort:{
                empty: boolean;
                unsorted: boolean;
                sorted: true;
            },
            offset:number;
            pageSize: number;
            pageNumber: number;
            paged: boolean
        },
        totalPages: number;
        totalElements: number;
        last: boolean;
        size: number;
        number: number;
        first: boolean;
        sort:{
            empty: boolean;
            unsorted: boolean;
            sorted: true;
        },
        numberOfElements: number;
        empty: boolean;
    };
}
