export interface AuthenticationResponse {

    error: boolean;
    requestStatus: number;
    token: string;
    
}
