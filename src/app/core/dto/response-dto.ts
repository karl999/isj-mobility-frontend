export interface ResponseDto {

    error: boolean;
    requestStatus: number;
}
