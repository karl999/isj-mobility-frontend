import { Time } from "@angular/common";
import { TimeClass } from "../../models/time/time-class";

export class PurchaseDto {

    constructor(){
        this.prixService = 0.0;
        this.produit = "";
        this.categorieProduit = "";
        this.quantite = 0;
        this.lieuAchat = "";
        this.lieuLivraison = "";
        this.prixUnitaire = 0.0;
        this.dateLivraison = new Date();
        this.horaireLivraison = new TimeClass();
    }
;
    
    categorieProduit: string;
    produit: string;
    lieuAchat: string;
    prixUnitaire: number;
    quantite: number;
    lieuLivraison: string;
    dateLivraison: Date;
    horaireLivraison: Time;
    prixService: number;
}
