import { Time } from "@angular/common";
import { TypeServiceEnum } from "../../enum/type-service";
import { Deserialize } from "../deserialize";
import { Service } from "../service";
import { TimeClass } from "../time/time-class";

export class Purchase implements Deserialize, Service {


    constructor(){
        this.typeService = TypeServiceEnum.PURCHASE
        this.code = "";
        this.prixService = 0.0;
        this.produit = "";
        this.categorieProduit = "";
        this.quantite = 0;
        this.lieuAchat = "";
        this.lieuLivraison = "";
        this.prixUnitaire = 0.0;
        this.dateLivraison = new Date();
        this.horaireLivraison = new TimeClass();
    }

    typeService: TypeServiceEnum;
    code: string;
    prixService: number;
    produit: string;
    categorieProduit: string;
    quantite: number;
    lieuAchat: string;
    lieuLivraison: string;
    prixUnitaire: number;
    dateLivraison: Date;
    horaireLivraison: Time;

    deserialize(input: any): this {
        Object.assign(this, input);
        this.dateLivraison = new Date(this.dateLivraison);
        this.horaireLivraison.hours = this.dateLivraison.getHours();
        this.horaireLivraison.minutes = this.dateLivraison.getMinutes();
        this.categorieProduit = input.categorieProduit.categorie;
        return this;
    }

}
