import { StatutReservation } from "../../enum/statut-reservation";
import { Deserialize } from "../deserialize";

export class Book implements Deserialize {

    constructor(){

        this.login = "";
        this.code = "",
        this.telephone = "";
        this.email = "";
        this.quantite = 0;
        this.statut = StatutReservation.EN_ATTENTE;
    }
    
    login: string;
    code:string;
    quantite: number;
    statut: StatutReservation;
    telephone: string;
    email: string;

    deserialize(input:{login:string, code:string, quantite:number, statut:StatutReservation, telephone: string, email: string}): this {
        Object.assign(this, input);
        this.statut = StatutReservation[this.statut];
        return this;
    }
    
}

