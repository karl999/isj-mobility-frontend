import { TypeServiceEnum } from "../enum/type-service";
import { TimeClass } from "./time/time-class";

export interface Service {

    typeService: TypeServiceEnum;
    code:string;
    prixService: number;
    
}
