import { Time } from "@angular/common";
import { TypeServiceEnum } from "../../enum/type-service";
import { Deserialize } from "../deserialize";
import { Service } from "../service";
import { TimeClass } from "../time/time-class";

export class Transport implements Deserialize, Service {

    constructor(){
        this.typeService =TypeServiceEnum.TRANSPORT
        this.code = "";
        this.prixService = 0.0;
        this.produit = "";
        this.categorieProduit = "";
        this.pointRamassage = "";
        this.quantite = 0;
        this.lieuLivraison = "";
        this.dateLivraison = new Date();
        this.horaireLivraison = new TimeClass();
    }

    typeService: TypeServiceEnum;
    code: string;
    prixService: number;
    produit: string;
    categorieProduit: string;
    pointRamassage: string;
    quantite: number;
    lieuLivraison: string;
    dateLivraison: Date;
    horaireLivraison: Time;

    deserialize(input: any): this {
        Object.assign(this, input);
        this.dateLivraison = new Date(this.dateLivraison);
        this.horaireLivraison.hours = this.dateLivraison.getHours();
        this.horaireLivraison.minutes = this.dateLivraison.getMinutes();
        this.categorieProduit = input.categorieProduit.categorie;
        return this;
    }

    getTime(which: string): String {
        return this.horaireLivraison.toString();
    }

}
