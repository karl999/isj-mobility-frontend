import { Time } from "@angular/common";

export class TimeClass implements Time {


    constructor(){
        this.hours = 0;
        this.minutes = 0;
    }

    hours: number;
    minutes: number;

    public toString(): String{
        if(this.hours < 10){
            if(this.minutes < 10){
                return "0" + this.hours + ":0" + this.minutes;
            }else{
                return "0" + this.hours + ":" + this.minutes;
            }
        }else{
            if(this.minutes < 10){  
                return this.hours + ":0" + this.minutes;
            }else{
                return this.hours + ":" + this.minutes;
            }
        }
    }

}

