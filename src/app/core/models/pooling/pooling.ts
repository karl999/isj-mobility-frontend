import { Time } from "@angular/common";
import { TypeCovoiturageEnum } from "../../enum/type-covoiturage-enum";
import { TypeServiceEnum } from "../../enum/type-service";
import { Deserialize } from "../deserialize";
import { Service } from "../service";
import { TimeClass } from "../time/time-class";

export class Pooling implements Deserialize, Service {

    constructor(){
        this.code = "";
        this.typeService = TypeServiceEnum.POOLING
        this.prixService = 0.0;
        this.type = TypeCovoiturageEnum.BUS;
        this.lieuDepart = "";
        this.lieuArrivee = "";
        this.placesDispo = 0;
        this.dateDepart = new Date();
        this.horaireDepart = new TimeClass();
    }

    typeService: TypeServiceEnum;
    code:string;
    type: TypeCovoiturageEnum;
    placesDispo: number;
    prixService: number;
    lieuDepart: string;
    dateDepart: Date ;
    horaireDepart: Time;
    lieuArrivee: string ;

    deserialize(input: any): this {
        Object.assign(this, input);
        this.dateDepart = new Date(this.dateDepart);
        this.horaireDepart.hours = this.dateDepart.getHours();
        this.horaireDepart.minutes = this.dateDepart.getMinutes();
        return this;
    }

    getTime(): String {
        return this.horaireDepart.toString();
    }

}
