import { Component } from '@angular/core';
import { KeyStorage } from './core/api-ressources/key-storage';
import { AuthenticationService } from './core/services/authentication/authentication.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private keyStorage: KeyStorage,
    private authService:AuthenticationService,
  ){
    this.authService.isAuthenticated.subscribe(
      value =>{
        this.isLogged = value;
        this.userLogin = localStorage.getItem(keyStorage.USER_LOGIN);
      }
    )
  }
  isLogged: boolean = false;

  userLogin: any = "";

  menus = [
    {
      title: "Accueil",
      path: "/home",
      icon: "home",
    },
    {
      title: "Nouvelle Offre",
      path: "/newpost",
      icon: "publish",
    },
    {
      title: "Rechercher",
      path: "/search",
      icon: "search",
    },
  ];
  hidden = false;

  toggleBadgeVisibility() {
    this.hidden = !this.hidden;
  }

  logout(){
    this.authService.logout();
  }
}
