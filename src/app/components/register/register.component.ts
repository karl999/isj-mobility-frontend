import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ErrorMessage } from 'src/app/core/api-ressources/error-message';
import { KeyStorage } from 'src/app/core/api-ressources/key-storage';
import { AuthenticationService } from 'src/app/core/services/authentication/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  Roles: any = ['Admin', 'Author', 'Reader'];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar,    

    private authService: AuthenticationService,
    private errorMessage: ErrorMessage,
    private keyStorage: KeyStorage,
  ) {
    this.signUpForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      sexe:  ['', [Validators.required]],
      login: ['', [Validators.required]],
      password: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      dateN: ['', [Validators.required]],
      telephone: ['', [Validators.required, Validators.pattern('^6[0-9]{8}$')]],
    });
  }

  signUpForm: FormGroup;

  ngOnInit(): void {
  }

  signUp(){
    if(this.signUpForm.valid){
      this.authService.signUp(this.signUpForm.value).subscribe(
        response => {
          if(!response.error){
            localStorage.setItem(this.keyStorage.TOKEN_NAME, response.token );
            this.authService.token = response.token;
            this.authService.isAuthenticated.next(true);
            this.router.navigateByUrl("/home", { replaceUrl: true });
          }
          this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], "Compris", {
            duration: 2000,
          });
        }
      );
    }
  }

  get name() {
    return this.signUpForm.get('name');
  }
  get firstName() {
    return this.signUpForm.get('firstName');
  }
  get login() {
    return this.signUpForm.get('login');
  }
  get password() {
    return this.signUpForm.get('password');
  }
  get email() {
    return this.signUpForm.get('email');
  }
  get dateN() {
    return this.signUpForm.get('dateN');
  }

  get telephone() {
    return this.signUpForm.get('telephone');
  }

  get sexe(){
    return this.signUpForm.get('sexe');
  }


}
