import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ErrorMessage } from 'src/app/core/api-ressources/error-message';
import { AuthenticationService } from 'src/app/core/services/authentication/authentication.service';
import { KeyStorage } from '../../core/api-ressources/key-storage';


@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar,    
    private authService: AuthenticationService,
    private errorMessage: ErrorMessage,
    private keyStorage: KeyStorage,
  ) {
    this.credentials = this.fb.group({
      login: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(4)]],
    });
   }

  hide = true;
  credentials: FormGroup;

  ngOnInit(): void {
  }

  signin(){
    this.authService.login(this.credentials.value).subscribe(
      response => {
        if(!response.error){
          localStorage.setItem(this.keyStorage.TOKEN_NAME, response.token);
          localStorage.setItem(this.keyStorage.USER_LOGIN, this.credentials.value.login);
          this.authService.token = response.token;
          this.authService.isAuthenticated.next(true);
          this.router.navigateByUrl("/home", { replaceUrl: true });
        }else{
          this.snackBar.open(this.errorMessage.messageCode[response.requestStatus]);
        }  
      }
    );    

  }

  // Easy access for form fields
  get login() {
    return this.credentials.get('login');
  }

  get password() {
    return this.credentials.get('password');
  }

}
