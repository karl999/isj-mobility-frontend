import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Pooling } from 'src/app/core/models/pooling/pooling';
import { Purchase } from 'src/app/core/models/purchase/purchase';
import { Transport } from 'src/app/core/models/transport/transport';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorMessage } from 'src/app/core/api-ressources/error-message';
import { ServiceService } from 'src/app/core/services/service/service.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SearchPatternService } from 'src/app/core/services/searchpattern/search-pattern.service';
import { TypeServiceEnum } from 'src/app/core/enum/type-service';
import { TypeCovoiturageEnum } from 'src/app/core/enum/type-covoiturage-enum';
import { BookService } from 'src/app/core/services/book/book.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  services: 'covoiturage' | 'achat' | 'transport' | '' = '';
  covoiturage: Boolean = false;
  achat: Boolean = false;
  transport: Boolean = false;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  max = 0;
  categories=[
    {
    
      valeur: "Alimentaire"
    },
    {
    
      valeur: "vetement"
    },
    {
    
      valeur: "cosmétique"
    },
    {
    
      valeur: "electromenager"
    },
    {
    
      valeur: "electronique"
    },
  ];

  typeco=["moto","voiture","bus"];

  lieu=["eyang","nkolbisson","meec"];

  poolingForm: FormGroup;
  purchaseForm: FormGroup;
  transportForm: FormGroup;
  
  poolingOffersList: Pooling[] = [];
  purchaseOffersList: Purchase[] = [];
  transportOffersList: Transport[] = [];
  serviceOffersList: any[] = [];

  isPoolingSearch = false;
  isPurchaseSearch = false;
  isTransportSearch = false;
  isServicesSearch = false;

  searchPattern: string = "";

  typeServiceEnum = {
    pooling: TypeServiceEnum.POOLING,
    purchase: TypeServiceEnum.PURCHASE,
    transport: TypeServiceEnum.TRANSPORT,
  };

  // For the pagination
  currentPage = 0;
  pageSize = 20;
  numberOfElements = 0;
  pageSizeOptions: number[] = [1,10, 20, 25, 50];

  constructor(
    private breakpointObserver: BreakpointObserver,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,    

    private bookService: BookService,
    private errorMessage: ErrorMessage,
    private notifService: NotificationService,
    private searchService: SearchPatternService,
    private service: ServiceService,
  ) {
    this.poolingForm = this.fb.group({
      typeCovoiturage: [''],
      lieuDepart: [''],
      dateDepart: [''],
      horaireDepart: [''],
      prixPlace: [''],
      lieuArrivee: [''],
      placesDispo: [''],

    });

    this.purchaseForm = this.fb.group({
      choixCategoriesAchat: [''],
      DateLivraison: [''], 
      horaireLivraison: [''],
      commisionTransport: [0],

    });
    this.transportForm = this.fb.group({
      choixCategoriesTransport: [''],
      DateLivraisonTransport: [''],
      horaireLivraisonTransport: [''],
      commisionTransport: [0],
    });

    this.listService(0,20);
  }



  disableOthers(service: String) {
    switch (service) {
      case 'achat':
        this.achat = true;
        this.covoiturage = false;
        this.transport = false;
        break;
      case 'covoiturage':
        this.achat = false;
        this.covoiturage = true;
        this.transport = false;
        break;
      case 'transport':
        this.achat = false;
        this.covoiturage = false;
        this.transport = true;
        break;
    }
  }

  stringifyTypePooling(serviceEnum: TypeCovoiturageEnum): string{
    return TypeCovoiturageEnum[serviceEnum];
  }
  stringifyDate(date: Date): string{
    return new Date(date).toLocaleDateString();
  }

  titleEnum(serviceEnum: TypeServiceEnum): string{
    switch (serviceEnum) {
      case TypeServiceEnum.POOLING:
        return "Offre Covoiturage";
        break;
      case TypeServiceEnum.PURCHASE:
        return "Offre d'Achat";
        break;
      case TypeServiceEnum.TRANSPORT:
        return "Offre de Transport";
        break;
    };
  }

  findPoolingOffers(){
    let data = {
      type: this.poolingForm.value.typeCovoiturage,
      prixService: this.poolingForm.value.prixPlace,
      placesDispo: this.poolingForm.value.placesDispo,
      lieuDepart: this.poolingForm.value.lieuDepartCovoiturage,
      dateDepart: this.poolingForm.value.dateDepartCovoiturage,
      horaireDepart: this.poolingForm.value.horaireDepartCovoiturage,
      lieuArrivee: this.poolingForm.value.lieuArriveeCovoiturage,
    }
    this.searchPattern = this.searchService.buildSearchPatternPooling(data);
    this.findPoolingOfferWithCriteria(this.currentPage,this.pageSize,this.searchPattern);
  }

  findPurchaseOffers(){
    let data = {
      categorieProduit: this.purchaseForm.value.choixCategoriesAchat,
      dateLivraison: this.purchaseForm.value.DateLivraison,
      horaireLivraison: this.purchaseForm.value.horaireLivraison,
    }
    this.searchPattern = this.searchService.buildSearchPatternPurchase(data);  
    this.findPurchaseOffersWithCriteria(this.currentPage,this.pageSize,this.searchPattern);
  }


  findTransportOffers(){
    let data = {
      categorieProduit: this.transportForm.value.choixCategoriesTransport,
      dateLivraison: this.transportForm.value.dateArriveeTransport,
      horaireLivraison: this.transportForm.value.horaireArriveeTransport,        
      prixService: this.transportForm.value.prixServiceTransport,
    };
    this.searchPattern = this.searchService.buildSearchPatternTransport(data);
    this.findTransportOffersWithCriteria(this.currentPage,this.pageSize,this.searchPattern);
  }

  findPoolingOfferWithCriteria(page: number = 0, limit: number = 20, pattern: string){
    this.isPoolingSearch = true;
    this.isPurchaseSearch = false;
    this.isTransportSearch = false;
    this.isServicesSearch = false;
    this.serviceOffersList = [];
    this.poolingOffersList = []
    this.purchaseOffersList = [];
    this.transportOffersList = [];
    let pooling: Pooling;
    this.service.findPoolingOffer(page, limit, pattern).subscribe(response => {
      if(!response.error){
          pooling = new Pooling();
          response.offers.content.forEach(po =>{
          pooling.deserialize(po);
          this.poolingOffersList.push(pooling);
        })
        this.currentPage = response.offers.number;
        this.pageSize = response.offers.size;
        this.numberOfElements = response.offers.totalElements;
      }else{
        this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], "Compris", {
          duration: 2000,
        });      
      }
    });
  }

  findPurchaseOffersWithCriteria(page: number = 0, limit: number = 20, pattern: string){
    this.isPoolingSearch = false;
    this.isPurchaseSearch = true;
    this.isTransportSearch = false;
    this.isServicesSearch = false;
    this.serviceOffersList = [];
    this.poolingOffersList = []
    this.purchaseOffersList = [];
    this.transportOffersList = [];
    let purchase: Purchase = new Purchase();
    this.service.findPurchaseOffer(page, limit, pattern).subscribe(response => {
      if(!response.error){
          response.offers.content.forEach(purchaseOffer =>{
            purchase = new Purchase();
            this.purchaseOffersList.push(purchase.deserialize(purchaseOffer));
        });
        this.currentPage = response.offers.number;
        this.pageSize = response.offers.size;
        this.numberOfElements = response.offers.totalElements;
      }else{
        this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], "Compris", {
          duration: 2000,
        });
      }
    });
  }

  findTransportOffersWithCriteria(page: number = 1, limit: number = 20, pattern: string){
    this.isPoolingSearch = false;
    this.isPurchaseSearch = false;
    this.isTransportSearch = true;
    this.isServicesSearch = false;
    this.serviceOffersList = [];
    this.poolingOffersList = []
    this.purchaseOffersList = [];
    this.transportOffersList = [];
    let transport: Transport;
    this.service.findTransportOffer(page, limit, pattern).subscribe(response => {
      if(!response.error){
          transport = new Transport();
          response.offers.content.forEach(tr =>{
          transport.deserialize(tr);
          this.transportOffersList.push(transport);
        });
        this.currentPage = response.offers.number;
        this.pageSize = response.offers.size;
        this.numberOfElements = response.offers.totalElements;
      }
      this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], "Compris", {
        duration: 2000,
      });
    });
  }

  listService(page: number = 0, limit: number = 20){
    this.isPoolingSearch = false;
    this.isPurchaseSearch = false;
    this.isTransportSearch = false;
    this.isServicesSearch = true;
    let pooling: Pooling = new Pooling();
    let purchase: Purchase = new Purchase();
    let transport: Transport = new Transport();
    this.serviceOffersList = [];
    this.service.listServicesOffer(page, limit).subscribe(response => {
      if(!response.error){
        response.offers.content.forEach(service =>{
          pooling = new Pooling();
          purchase = new Purchase();
          transport = new Transport();
          console.log(service);
          if(service.hasOwnProperty("placesDispo")){
            this.serviceOffersList.push(pooling.deserialize(service));
          }else {
            if(service.hasOwnProperty("prixUnitaire")){
              this.serviceOffersList.push(purchase.deserialize(service));
            }else{
              this.serviceOffersList.push(transport.deserialize(service));
            }
          }
        });
        this.currentPage = response.offers.number;
        this.pageSize = response.offers.size;
        this.numberOfElements = response.offers.totalElements;
      }else{
        this.snackBar.open(this.errorMessage.messageCode[response.requestStatus],'Ok',{
          duration: 2000,
        });
      }
    });
  }

  book(code: string){
    this.bookService.book(code).subscribe(
      response =>{
        this.snackBar.open(this.errorMessage.messageCode[response.requestStatus],'Ok',{
          duration: 2000,
        });
      }
    );
  }

  follow(login: string){
    this.notifService.follow(login).subscribe(
      response =>{
        this.snackBar.open(this.errorMessage.messageCode[response.requestStatus],'Ok',{
          duration: 2000,
        });
      }
    );
  }

  changePage(pageEvent: PageEvent){
    if(this.isPoolingSearch){
      this.findPoolingOfferWithCriteria(pageEvent.pageIndex, pageEvent.pageSize, this.searchPattern);
    }
    if(this.isPurchaseSearch){
      this.findPurchaseOffersWithCriteria(pageEvent.pageIndex, pageEvent.pageSize, this.searchPattern);
    }
    if(this.isTransportSearch){
      this.findTransportOffersWithCriteria(pageEvent.pageIndex, pageEvent.pageSize, this.searchPattern);
    }
    if(this.isServicesSearch){
      this.listService(pageEvent.pageIndex, pageEvent.pageSize);
    }
  }

  // Pooling form
  get typeCovoiturage(){
    return this.poolingForm.get('typeCovoiturage');
  }

  get lieuDepart(){
    return this.poolingForm.get('lieuDepart');
  }

  get dateDepart(){
    return this.poolingForm.get('dateDepart');
  }

  get horaireDepart(){
    return this.poolingForm.get('typeCovoiturage');
  }

  get prixPlace(){
    return this.poolingForm.get('prixPlace');
  }

  get lieuArrivee(){
    return this.poolingForm.get('lieuArrivee');
  }

  get placesDispo(){
    return this.poolingForm.get('placesDispo');
  }


  // Purchase form
  get choixCategoriesAchat(){
    return this.purchaseForm.get('choixCategoriesAchat');
  }

  get DateLivraison(){
    return this.purchaseForm.get('DateLivraison');
  }

  get horaireLivraison(){
    return this.purchaseForm.get('horaireLivraison');
  }

  // transport form
  get choixCategoriesTransport(){
    return this.transportForm.get('choixCategoriesTransport');
  }

  get DateLivraisonTransport(){
    return this.transportForm.get('DateLivraisonTransport');
  }

  get horaireLivraisonTransport(){
    return this.transportForm.get('horaireLivraisonTransport');
  }


}
