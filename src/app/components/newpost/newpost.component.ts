import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from 'src/app/core/services/service/service.service';

import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorMessage } from 'src/app/core/api-ressources/error-message';
import { PoolingDto } from 'src/app/core/dto/pooling/pooling-dto';
import { PurchaseDto } from 'src/app/core/dto/purchase/purchase-dto';
import { TransportDto } from 'src/app/core/dto/transport/transport-dto';

@Component({
  selector: 'app-newpost',
  templateUrl: './newpost.component.html',
  styleUrls: ['./newpost.component.scss'],
})
export class NewpostComponent implements OnInit {
  services: 'covoiturage' | 'achat' | 'transport' | '' = '';
  covoiturage: Boolean = false;
  achat: Boolean = false;
  transport: Boolean = false;

  choixSelectCatego: string[] = ["ALIMENTAIRE", "SANTE", "COSMETIQUE", "VETEMENT", "ELECTRONIQUE", "ELECTROMENAGER"];
  choixSelectCov: string[]= ["MOTO", "VOITURE", "BUS"];
  dataPooling: PoolingDto = new PoolingDto();
  dataPurchase: PurchaseDto = new PurchaseDto();
  dataTransport: TransportDto = new TransportDto();
  // name: any; lieuDepart: any; mobile: any; subject: any; message: any

  poolingForm: FormGroup;
  purchaseForm: FormGroup;
  transportForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar,

    private errorMessage: ErrorMessage,
    private serviceService: ServiceService,
  ) {
    this.poolingForm = this.formBuilder.group({
      typeCovoiturage: ['', [Validators.required]],
      prixPlace: ['', [Validators.required]],
      placesDispo: ['', [Validators.required]],
      lieuDepartCovoiturage: ['', [Validators.required]],
      dateDepartCovoiturage: ['', [Validators.required]],
      horaireDepartCovoiturage: ['', [Validators.required]],
      lieuArriveeCovoiturage: ['', [Validators.required]],  
    });

    this.purchaseForm = this.formBuilder.group({
      choixCategoriesAchat: ['', [Validators.required]],
      produitAchat: ['', [Validators.required]], 
      lieuAchat: ['', [Validators.required]],
      prixProduitAchat: ['', [Validators.required]],
      quantiteProduitAchat: ['', [Validators.required]],
      lieuArriveeAchat: ['', [Validators.required]],   
      dateArriveeAchat: ['', [Validators.required]],
      horaireArriveeAchat: ['', [Validators.required]],
      prixServiceAchat: ['', [Validators.required]],
    });
    this.transportForm = this.formBuilder.group({
      choixCategoriesTransport: ['', [Validators.required]],
      produitTransport: ['', [Validators.required]],
      quantiteProduitTransport: ['', [Validators.required]],
      pointRamassage: ['', [Validators.required]],
      lieuArriveeTransport: ['', [Validators.required]],
      dateArriveeTransport: ['', [Validators.required]],
      horaireArriveeTransport: ['', [Validators.required]],  
      prixServiceTransport: ['', [Validators.required]],
    });
  }


  ngOnInit() {}


  disableOthers(service: String) {
    switch (service) {
      case 'achat':
        this.achat = true;
        this.covoiturage = false;
        this.transport = false;
        break;
      case 'covoiturage':
        this.achat = false;
        this.covoiturage = true;
        this.transport = false;
        break;
      case 'transport':
        this.achat = false;
        this.covoiturage = false;
        this.transport = true;
        break;
    }
  }

  savePoolingOffer(): void {
    if(this.poolingForm.valid){
      this.dataPooling = {
        type: this.poolingForm.value.typeCovoiturage,
        prixService: this.poolingForm.value.prixPlace,
        placesDispo: this.poolingForm.value.placesDispo,
        lieuDepart: this.poolingForm.value.lieuDepartCovoiturage,
        dateDepart: this.poolingForm.value.dateDepartCovoiturage,
        horaireDepart: this.poolingForm.value.horaireDepartCovoiturage,
        lieuArrivee: this.poolingForm.value.lieuArriveeCovoiturage,
      };
      this.serviceService.publishPoolingOffer(this.dataPooling).subscribe(
        response => {
          this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], 'Compris', {
            duration: 2000,
          });
          if(!response.error){
            this.router.navigateByUrl("/home"); 
          }
        }
      );
    }
  }

  savePurchaseOffer(): void {
    if(this.purchaseForm.valid){
      this.dataPurchase = {
        categorieProduit: this.purchaseForm.value.choixCategoriesAchat,
        produit: this.purchaseForm.value.produitAchat,
        lieuAchat: this.purchaseForm.value.lieuAchat,
        prixUnitaire: this.purchaseForm.value.prixProduitAchat,
        quantite: this.purchaseForm.value.quantiteProduitAchat,
        lieuLivraison: this.purchaseForm.value.lieuArriveeAchat,
        dateLivraison: this.purchaseForm.value.dateArriveeAchat,
        horaireLivraison: this.purchaseForm.value.horaireArriveeAchat,
        prixService: this.purchaseForm.value.prixServiceAchat,  
      }
      this.serviceService.publishPurchaseOffer(this.dataPurchase).subscribe(
        response => {
          this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], 'Compris', {
            duration: 2000,
          });
          if(!response.error){
            this.router.navigateByUrl("/home"); 
          }
        }
      );
    }
    
  }

  saveTransportOffer(): void {
    if(this.transportForm.valid){
      this.dataTransport = {
        categorieProduit: this.transportForm.value.choixCategoriesTransport,
        produit: this.transportForm.value.produitTransport,
        quantite: this.transportForm.value.quantiteProduitTransport,
        pointRamassage: this.transportForm.value.pointRamassage,
        lieuLivraison: this.transportForm.value.lieuArriveeTransport,
        dateLivraison: this.transportForm.value.dateArriveeTransport,
        horaireLivraison: this.transportForm.value.horaireArriveeTransport,        
        prixService: this.transportForm.value.prixServiceTransport,
      }
      this.serviceService.publishtransportOffer(this.dataTransport).subscribe(
        response => {
          this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], 'Compris', {
            duration: 2000,
          });
          if(!response.error){
            this.router.navigateByUrl("/home"); 
          }
        }
      );
    } 
  }

  // poolingForm
  get typeCovoiturage(){
    return this.poolingForm.get('typeCovoiturage');
  }

  get lieuDepartCovoiturage(){
    return this.poolingForm.get('lieuDepartCovoiturage');
  }
  get dateDepartCovoiturage(){
    return this.poolingForm.get('dateDepartCovoiturage');
  }

  get horaireDepartCovoiturage(){
    return this.poolingForm.get('horaireDepartCovoiturage');
  }

  get lieuArriverCovoiturage(){
    return this.poolingForm.get('lieuArriverCovoiturage');
  }

  get prixPlace(){
    return this.poolingForm.get('prixPlace');
  }

  get placesReservees(){
    return this.poolingForm.get('placesReservees');
  }


  // PurchaseForm
  get lieuArriveeAchat(){
    return this.purchaseForm.get('lieuArriveeAchat');
  }
  get dateArriveeAchat(){
    return this.purchaseForm.get('dateArriveeAchat');
  }
  get lieuAchat(){
    return this.purchaseForm.get('lieuAchat');
  }
  get produitAchat(){
    return this.purchaseForm.get('produitAchat');
  }
  get choixCategoriesAchat(){
    return this.purchaseForm.get('choixCategoriesAchat');
  }
  get prixProduitAchat(){
    return this.purchaseForm.get('prixProduitAchat');
  }
  
  get quantiteProduitAchat(){
    return this.purchaseForm.get('quantiteProduitAchat');
  }

  get prixServiceAchat(){
    return this.purchaseForm.get('prixServiceAchat');
  }

  get horaireArriveeAchat(){
    return this.purchaseForm.get('horaireArriveeAchat');
  }


  // transportForm
  get lieuArriveeTransport(){
    return this.transportForm.get('lieuArriveeTransport');
  }

  get dateArriveeTransport(){
    return this.transportForm.get('dateArriveeTransport');
  }

  get horaireArriveeTransport(){
    return this.transportForm.get('horaireArriveeTransport');
  }

  get produitTransport(){
    return this.transportForm.get('produitTransport');
  }

  get choixCategoriesTransport(){
    return this.transportForm.get('choixCategoriesTransport');
  }

  get quantiteProduitTransport(){
    return this.transportForm.get('quantiteProduitTransport');
  }

  get pointRamassage(){
    return this.transportForm.get('pointRamassage');
  }

  get prixServiceTransport(){
    return this.transportForm.get('prixServiceTransport');
  }

}
