import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notifs',
  templateUrl: './notifs.component.html',
  styleUrls: ['./notifs.component.scss']
})
export class NotifsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  objetsnotifs: string[] = ['Validation de votre reservation de covoiturage du 10/03/22 a 12h', 'Rejet de votre reservation de covoiturage du 09/03/22 a 12h', 'Reaction a votre proposition de service achat du 10/03/22 a 14h', 'Validation de votre reservation de covoiturage du 12/03/22 a 12h', 'Reaction a votre sollicitation de Transport du 10/03/22 a 12h'];

}
