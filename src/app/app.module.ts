import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material-module/angular-material.module';

import { HttpClient, HttpClientModule } from "@angular/common/http";
import { AdminModule } from './admin/admin.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './components/home/home.component';
import { NewpostComponent } from './components/newpost/newpost.component';
import { SearchComponent } from './components/search/search.component';
import { RegisterComponent } from './components/register/register.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { CoreModule } from './core/core.module';
import { LandingComponent } from './components/landing/landing.component';
import { NotifsComponent } from './components/notifs/notifs.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewpostComponent,
    SearchComponent,
    RegisterComponent,
    LogInComponent,
    LandingComponent,
    NotifsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    HttpClientModule,
    AdminModule,
    ReactiveFormsModule,
    FormsModule,
    CoreModule,
  ],
  providers: [
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
