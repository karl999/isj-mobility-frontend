import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ListBookComponent } from './list-book/list-book.component';
import { ListOffersComponent } from './list-offers/list-offers.component';

const routes: Routes = [
  {
    path: 'offer',
    component: ListOffersComponent
  },
  {
    path: 'book/:code',
    component: ListBookComponent,
  },
  { 
    path: '', 
    component: AdminComponent 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
