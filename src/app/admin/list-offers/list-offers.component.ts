import { Component, OnInit } from '@angular/core';

import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { ErrorMessage } from 'src/app/core/api-ressources/error-message';
import { KeyStorage } from 'src/app/core/api-ressources/key-storage';
import { TypeCovoiturageEnum } from 'src/app/core/enum/type-covoiturage-enum';
import { TypeServiceEnum } from 'src/app/core/enum/type-service';
import { Pooling } from 'src/app/core/models/pooling/pooling';
import { Purchase } from 'src/app/core/models/purchase/purchase';
import { Transport } from 'src/app/core/models/transport/transport';
import { ServiceService } from 'src/app/core/services/service/service.service';

@Component({
  selector: 'app-list-offers',
  templateUrl: './list-offers.component.html',
  styleUrls: ['./list-offers.component.scss']
})
export class ListOffersComponent implements OnInit {

  servicesList: Array<any> = [];
  poolingOffersList: Pooling[] = [];
  purchaseOffersList: Purchase[] = [];
  transportOffersList: Transport[] = [];

  typeServiceEnum = {
    pooling: TypeServiceEnum.POOLING,
    purchase: TypeServiceEnum.PURCHASE,
    transport: TypeServiceEnum.TRANSPORT,
  };

  sortOptions = {
    order: {
      increasing: "asc",
      decreasing: "desc"
    },
    typeService: {
      pooling: "pooling",
      purchase: "purchase",
      transport: "transport"
    },
    typeTransport: {
      bus: "BUS",
      moto: "MOTO",
      voiture: "VOITURE"
    },
    categorieProduit: {
      alimentaire: "ALIMENTAIRE",
      cosmetique: "COSMETIQUE",
      electromenager: "ELECTROMENAGER",
      electronique: "ELECTRONIQUE",
      vetements: "VETEMENTS"
    }
  };

  bookSort = {
    prixService: "",
    orderDate: "",
    typeService: "",
    typeTransport: "",
    categorieProduit: "",
  };

  // if true the offer is a pooling offer false otherwise
  typeOffer: boolean = false;

  constructor(
    private errorMessage: ErrorMessage,
    private keyStorage: KeyStorage,
    private service: ServiceService,

    private router: Router,
    private snackBar: MatSnackBar,    
  ) {}

  ngOnInit(): void {
    this.listService(0, 20);
  }

  changeTypeOfferValue(){  
    if(this.bookSort.typeService == this.sortOptions.typeService.pooling){
      this.typeOffer = true;
    }else{
      this.typeOffer = false;
    }
  }

  stringifyTypePooling(serviceEnum: TypeCovoiturageEnum): string{
    return TypeCovoiturageEnum[serviceEnum];
  }
  stringifyDate(date: Date): string{
    return new Date(date).toLocaleDateString();
  }

  titleEnum(serviceEnum: TypeServiceEnum): string{
    switch (serviceEnum) {
      case TypeServiceEnum.POOLING:
        return "Offre Covoiturage";
        break;
      case TypeServiceEnum.PURCHASE:
        return "Offre d'Achat";
        break;
      case TypeServiceEnum.TRANSPORT:
        return "Offre de Transport";
        break;
    };
  }

  deleteOffer(service: Pooling|Purchase|Transport){
    switch (service.typeService) {
      case TypeServiceEnum.POOLING:
        this.deletePoolingOffer(service.code);
        break;
      case TypeServiceEnum.PURCHASE:
        this.deletePurchaseOffer(service.code);
        break;
      case TypeServiceEnum.TRANSPORT:
        this.deletetransportOffer(service.code);
        break;
    };
  }

  deletePoolingOffer(code: string){
    this.service.deletePoolingOffer(code).subscribe( response => {
      this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], "Compris", {
        duration: 2000,
      });
      if(!response.error){
        this.listService(1, 20);
      }
    });
  }

  deletePurchaseOffer(code: string){
    this.service.deletePurchaseOffer(code).subscribe( response => {
      this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], "Compris", {
        duration: 2000,
      });    
      if(!response.error){
        this.listService(1, 20);
      }
    });
  }

  deletetransportOffer(code: string){
    this.service.deletetransportOffer(code).subscribe( response => {
      this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], "Compris", {
        duration: 2000,
      });    
      if(!response.error){
        this.listService(1, 20);
      }
    });
  }

  findPoolingOffer(page: number = 0, limit: number = 20){
    this.poolingOffersList = [];
    let pooling: Pooling;
    this.service.findPoolingOffer(page, limit).subscribe(response => {
      if(!response.error){
          response.offers.content.forEach(po =>{
          pooling.deserialize(po);
          this.poolingOffersList.push(pooling);
        })
      }
      this.snackBar.open(this.errorMessage.messageCode[response.requestStatus]);
    });
  }

  findPurchaseOffer(page: number = 0, limit: number = 20){
    this.purchaseOffersList = [];
    let purchase: Purchase;
    this.service.findPurchaseOffer(page, limit).subscribe(response => {
      if(!response.error){
          response.offers.content.forEach(pu =>{
          purchase.deserialize(pu);
          this.purchaseOffersList.push(purchase);
        })
      }
      this.snackBar.open(this.errorMessage.messageCode[response.requestStatus]);

    });
  }

  findTransportOffer(page: number = 0, limit: number = 20){
    this.transportOffersList = [];
    let transport: Transport;
    this.service.findTransportOffer(page, limit).subscribe(response => {
      if(!response.error){
          response.offers.content.forEach(tr =>{
          transport.deserialize(tr);
          this.transportOffersList.push(transport);
        })
      }
      this.snackBar.open(this.errorMessage.messageCode[response.requestStatus], "Compris", {
        duration: 2000,
      });
    });
  }

  listService(page: number = 0, limit: number = 20){
    let pooling: Pooling = new Pooling();
    let purchase: Purchase = new Purchase();
    let transport: Transport = new Transport();
    this.servicesList = [];
    this.service.listServicesOwnerOffer(page, limit).subscribe(response => {
      if(!response.error){
        response.offers.content.forEach(service =>{
          pooling = new Pooling();
          purchase = new Purchase();
          transport = new Transport();
          if(service.hasOwnProperty("placesDispo")){
            this.servicesList.push(pooling.deserialize(service));
          }else {
            if(service.hasOwnProperty("prixUnitaire")){
              this.servicesList.push(purchase.deserialize(service));
            }else{
              this.servicesList.push(transport.deserialize(service));
            }
          }

        })
      }else{
        this.snackBar.open(this.errorMessage.messageCode[response.requestStatus],'Ok',{
          duration: 2000,
        });
      }
    });
  }

  navigateToBook(service:any){
    localStorage.setItem(this.keyStorage.BOOKS_SERVICE_KEY, JSON.stringify(service));
    this.router.navigate(["/admin/book", service.code]);
  }

}
