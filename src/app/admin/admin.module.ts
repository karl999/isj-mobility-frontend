import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { ListOffersComponent } from './list-offers/list-offers.component';
import { AngularMaterialModule } from '../angular-material-module/angular-material.module';

import { CoreModule } from '../core/core.module';
import { ListBookComponent } from './list-book/list-book.component';


@NgModule({
  declarations: [
    AdminComponent,
    ListOffersComponent,
    ListBookComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    AngularMaterialModule,
    CoreModule,
  ],
})
export class AdminModule { }
