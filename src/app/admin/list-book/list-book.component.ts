import { Component, OnInit } from '@angular/core';

import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';

import { ErrorMessage } from 'src/app/core/api-ressources/error-message';
import { StatutReservation } from 'src/app/core/enum/statut-reservation';
import { Book } from 'src/app/core/models/book/book.model';
import { BookService } from 'src/app/core/services/book/book.service';

@Component({
  selector: 'app-list-book',
  templateUrl: './list-book.component.html',
  styleUrls: ['./list-book.component.scss']
})
export class ListBookComponent implements OnInit {

  constructor(
    private errorMessage: ErrorMessage,
    private bookService: BookService,
    private route: ActivatedRoute,

    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    let code= this.route.snapshot.paramMap.get('code');
    if(code != null){
      this.code = code;
    }
    this.listBooks(this.code);
  }

  code: string = "";

  booksList: Array<Book> = [];

  statutBook:any = {
    accept: StatutReservation.ACCEPTE,
    wait: StatutReservation.EN_ATTENTE,
    reject: StatutReservation.REFUSE,
    ACCEPTE: StatutReservation.ACCEPTE,
    EN_ATTENTE: StatutReservation.EN_ATTENTE,
    REFUSE: StatutReservation.REFUSE
  }

  // This is the possible sorting order
  orderDate = {
    increasing: "asc",
    decreasing: "desc"
  };

  // It is the actual sort for the var
  bookSort = {
    status: "",
    orderDate: "",
  };

  panelOpenState:boolean = false;

  stringifyStatutBook(serviceEnum: StatutReservation): string{
    switch(serviceEnum){
      case StatutReservation.ACCEPTE:
        return "Accepté";
        break;
      case StatutReservation.EN_ATTENTE:
        return "En Attente";
        break;
      case StatutReservation.REFUSE:
        return "Refusé";
        break;
    }
  }

  confirmBook(c: string, statut: StatutReservation){
    this.bookService.confirmBook({c, statut}).subscribe(
      response =>{
        this.snackBar.open(this.errorMessage.messageCode[response.requestStatus],'Ok',{
          duration: 2000,
        });      
      }
    );
    this.listBooks(this.code);
  }

  deleteBook(code: string){
    this.bookService.deleteBook(code).subscribe(
      response =>{
        this.snackBar.open(this.errorMessage.messageCode[response.requestStatus],'Ok',{
          duration: 2000,
        });      
      }
    );
    this.listBooks(this.code);
  }

  listBooks(code: string){
    this.booksList = [];
    let book: Book = new Book();
    this.bookService.listBook(code).subscribe(
      response => {
        if(!response.error){
          response.books.forEach(reservation =>{
            this.booksList.push(book.deserialize(reservation));
          })
        }
      }
    );
  }

  test(mes:string,t:any){
    console.log(mes + typeof t);
  }

}
